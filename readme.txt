Team : Gone4Green

Nous avons pris le parti de réaliser ce projet avec Spring côté Serveur et Jquery côté Client.

L'objectif étant d'offrir une expérience utilisateur agréable et de stocker les données dans une base côté serveur pour pouvoir proposer l'agrégation des données
par la suite.

Notre interface est directement disponible depuis le serveur à l'adresse suivante :

http://vps613147.ovh.net:8092


Les sources de ce challenge, reste la propriété de l'équipe de développeurs Gone4Green :
- Julien BARBERAN
- Brigitte BELTRAMO
- Sylvain JOUARDET
- Benjamin PARTOUCHE-SEBBAN
- Nabil ZAINI