#!/bin/bash

set -e -u -x

cd source-code/
./mvnw clean package -Dmaven.test.skip=true
ls -l
mkdir build-output
cp target/*.jar build-output
cp ci/tasks/deploy.sh build-output
