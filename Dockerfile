FROM openjdk:8-jdk-alpine
ADD . /
RUN ls /
RUN ./mvnw clean package -Dmaven.test.skip=true
RUN ls
RUN mv target/survey-0.0.1-SNAPSHOT.jar survey.jar
RUN rm -rf Dockerfile ic mvnw mvnw.cmd pom.xml target
EXPOSE 8092:8092
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/survey.jar"]