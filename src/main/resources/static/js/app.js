var $container = null
var currentQuestion = null;
var previousQuestion = 0;
var surveys = [];
var user = new Object();
var request = new Object();
var survey = new Object();
var idNextQuestion = 0;
var firstQuestion = 1;
var isSubmit = false;
// Permet de charger les données du sondage
const showTopics = id => {

    const q = topics[id]
    document.getElementById("app").innerHTML = renderTopic(q);
    addEvents()

    survey = JSON.parse(window.localStorage.getItem(currentQuestion.id));
    if (survey != null) {
        if(survey.idResponse != null){
            if(document.getElementById(survey.idResponse) !== null){
                document.getElementById(survey.idResponse).checked = true;
            }
            console.log("uservalue : " + survey.userValue);
            console.log("idreponse : " + survey.idResponse);
            if(survey.userValue != null && survey.userValue !== ""){
                if(document.getElementById(survey.idResponse+"_text") !== null){
                    document.getElementById(survey.idResponse+"_text").value=survey.userValue;
                }
            }
            $("#suivant").removeAttr("disabled");
        }
    }

}

const renderTopic = ({question, responses}) => {
  currentQuestion = question;
  window.localStorage.setItem("current", currentQuestion.id);

  if (currentQuestion.id == 1) {
      $("#precedent").attr("disabled", true);
      $("#share").attr("disabled", true);
  } else {
      $("#precedent").attr("disabled", false);
      $("#share").attr("disabled", false);
  }

  if (currentQuestion.id == topics.taille) {
      $("#send").attr("disabled", false);
      $("#share").attr("disabled", true);
      $("#suivant").attr("disabled", true);
  } else {
      $("#send").attr("disabled", true);
      $("#share").attr("disabled", false);
      $("#suivant").attr("disabled", false);
  }

  responses = (responses || []).map(renderResponse).join('')

    switch (question.category_id) {
        case 13: document.getElementById("categorie").textContent="Your compagny";document.getElementById("categorie").style.color="#e5528f";break;
        case 54: document.getElementById("categorie").textContent="Infrastructures";document.getElementById("categorie").style.color="#135380";break;
        case 137: document.getElementById("categorie").textContent="Management";document.getElementById("categorie").style.color="#975ca5";break;
        case 168: document.getElementById("categorie").textContent="Workstation ";document.getElementById("categorie").style.color="#e68125";break;
        case 200: document.getElementById("categorie").textContent="Energy consumption";document.getElementById("categorie").style.color="#e74e3d";break;
        case 218: document.getElementById("categorie").textContent="Printing";document.getElementById("categorie").style.color="#f19c06";break;
    }

  return `<b>${question.id}- ${question.label}</b><br/><i>${question.help}</i><ul><br/>${responses}<u/>`
}

const renderResponse = ({label, idNextQuestion, id, typeComponent, help}) => {
    var expr = typeComponent;
    switch (expr) {

        case 'CS': return `<div><input id="${id}" type="radio"  name="group" href="#" class="answer" data-id="${id}" data-next-question="${idNextQuestion}"><label>&nbsp; ${label} <i style="color: #8f8f8f">&nbsp; ${help}</i></label></div>`
        case 'CM': return `<div><input id="${id}" type="checkbox" name="group"  href="#" class="answer" data-id="${id}" data-next-question="${idNextQuestion}"><label>&nbsp; ${label} <i style="color: #8f8f8f">&nbsp; ${help}</i> </label></div>`
        case 'Entier': return `<div><input id="${id}_text" type="text" name="group"  href="#" class="answer" data-id="${id}" data-next-question="${idNextQuestion}"><label>&nbsp; ${label} <i style="color: #8f8f8f">&nbsp; ${help}</i> </label></div>`
        case 'CS_Nombre':  return `<div><input id="${id}" type="radio"  name="group" href="#" class="answer" data-id="${id}" data-next-question="${idNextQuestion}"><label>&nbsp; ${label} <i style="color: #8f8f8f">&nbsp; ${help}</i></label></div>
                           <div><input id="${id}_text" type="text" name="group" href="#" class="answer" data-id="${id}" data-next-question="${idNextQuestion}"></div>`
        case 'CS_Text': return `<div><input id="${id}" type="radio"  name="group" href="#" class="answer" data-id="${id}" data-next-question="${idNextQuestion}"><label>&nbsp; ${label} <i style="color: #8f8f8f">&nbsp; ${help}</i></label></div>
                           <div><input id="${id}_text" type="text" name="group" href="#" class="answer" data-id="${id}" data-next-question="${idNextQuestion}"></div>`
        case 'CS_Pourcent': return `<div><input id="${id}" type="radio"  name="group" href="#" class="answer" data-id="${id}" data-next-question="${idNextQuestion}"><label>&nbsp; ${label} <i style="color: #8f8f8f">&nbsp; ${help}</i></label></div>
                           <div><input id="${id}_text" type="text" name="group" href="#" class="answer" data-id="${id}" data-next-question="${idNextQuestion}"></div>`
        case 'CS_Entier':  return `<div><input id="${id}" type="radio"  name="group" href="#" class="answer" data-id="${id}" data-next-question="${idNextQuestion}"><label>&nbsp; ${label} <i style="color: #8f8f8f">&nbsp; ${help}</i></label></div>
                           <div><input id="${id}_text" type="text" name="group" href="#" class="answer" data-id="${id}" data-next-question="${idNextQuestion}"></div>`
        default: return `<p>Not found</p>`
    }
}

const addEvents = () => {

  $('.answer').focusout(function() {
      $("#suivant").removeAttr("disabled");
      idNextQuestion = $(this).data('next-question')
      if (idNextQuestion == null) {
        idNextQuestion = currentQuestion.id + 1;
      }
      var survey = new Object();
    survey.idQuestion = currentQuestion.id;
    survey.idResponse = $(this).data('id');
    survey.idNextQuestion = idNextQuestion;
    survey.idPreviousQuestion = previousQuestion;
    var userVal = "";
      if(document.getElementById(survey.idResponse+"_text") !== null){
          if (document.getElementById(survey.idResponse+"_text").value !== ""){
              userVal = document.getElementById(survey.idResponse+"_text").value; // $('#txt_name').text();
          }
      }
    survey.userValue = userVal;

    window.localStorage.setItem(currentQuestion.id, JSON.stringify(survey));
    previousQuestion = currentQuestion.id;

  })
}

$(document).ready(function() {
    $("#suivant").attr("disabled", true);
  $container = $('#app')

    var nextQuestionValue = $("#nextQuestion").text();
  if (nextQuestionValue != '') {
      firstQuestion = nextQuestionValue;
  }

  var currentQuestion = window.localStorage.getItem("current");
    if (currentQuestion != null) {
        firstQuestion = currentQuestion;
    }
  showTopics(firstQuestion);

    var userUuid = $("#uuid").text();
    if (userUuid != '') {
        user.id = userUuid;
    }




    $("#save").submit(function (event) {
        isSubmit = true;
        //stop submit the form, we will post it manually.
        event.preventDefault();
        submitSurvey();
        fire_ajax_submit();
        window.localStorage.clear();
    });

    $("#precedent").click(function () {
        survey = JSON.parse(window.localStorage.getItem(previousQuestion));
        showTopics(survey.idQuestion);
        previousQuestion = survey.idPreviousQuestion;
    });

    $("#suivant").click(function () {
        $("#suivant").attr("disabled", true);
        showTopics(idNextQuestion);
    });

    $("#share").click(function (event) {
        if (!confirm("do you want to save the survey? you can continue it later")){
            return false;
        }
        //stop submit the form, we will post it manually.
        event.preventDefault();
        submitSurvey();
        fire_ajax_submit();

    });

})

// Permet de faire appel au serveur pour soumettre les données
function fire_ajax_submit(){

    // DO POST
    $.ajax({
        type : "POST",
        contentType : "application/json",
        url : "http://vps613147.ovh.net:8092/save",
        data : JSON.stringify(request),
        crosDomain : true,
        success : function(result) {
            alert("Here is the link to share your survey : \n" + result);
            user.id = result.substring(result.lastIndexOf("/") + 1, result.length);
        },
        error : function() {
            alert("Une erreur est survenue lors de l'appel au serveur!")
        }
    });
}

// Génère les données à envoyer au serveur
function submitSurvey() {

    var index = firstQuestion;
    while(index != null) {
        survey = JSON.parse(window.localStorage.getItem(index));
        if (survey != null) {
            surveys.push(survey);
            index = survey.idNextQuestion;
        } else {
            index = null;
        }
    }

    request.surveys = surveys;
    request.user = user;
    if(isSubmit){
        document.getElementById("postResultDiv").style.visibility = "visible";
    }

}