// Contenu du sondage à afficher dans la page principale
const topics = {
    taille:88,
    1: {
        "question": {
            "id": 1,
            "category_id": 13,
            "label": " In which sector of activity is your company active? (linked with your APE code)",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 14,
                "typeComponent": "CS",
                "label": "Industry",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 15,
                "typeComponent": "CS",
                "label": "Business",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 16,
                "typeComponent": "CS",
                "label": "Information and communication",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 17,
                "typeComponent": "CS",
                "label": "Other services activity",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 18,
                "typeComponent": "CS",
                "label": "All other activities (financial, agricultural activities ...)",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 19,
                "typeComponent": "CS",
                "label": "Construction ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 20,
                "typeComponent": "CS",
                "label": "Public sector ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 21,
                "typeComponent": "CS",
                "label": "Specialized, Scientific and Technical or Administrative and Support Services Activities",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 22,
                "typeComponent": "CS",
                "label": "Real estate",
                "help": ""
            }
        ]
    },
    2: {
        "question": {
            "id": 2,
            "category_id": 13,
            "label": " What is the number of employees in your company on 18/12/31 ?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 25,
                "typeComponent": "CS",
                "label": "0",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 26,
                "typeComponent": "CS",
                "label": "1 to 2",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 27,
                "typeComponent": "CS",
                "label": "3 to 9",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 28,
                "typeComponent": "CS",
                "label": "10 to 49",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 29,
                "typeComponent": "CS",
                "label": "50 to 249",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 30,
                "typeComponent": "CS",
                "label": "250 to 4999",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 31,
                "typeComponent": "CS",
                "label": "More than 5000",
                "help": ""
            }
        ]
    },
    3: {
        "question": {
            "id": 3,
            "category_id": 13,
            "label": " What is the turnover of your company in the last fiscal year? (or annual budget for Public sector)",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 34,
                "typeComponent": "CS",
                "label": "0 to 100K €",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 35,
                "typeComponent": "CS",
                "label": "100 to 500 K€",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 36,
                "typeComponent": "CS",
                "label": "500 to 2 M €",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 37,
                "typeComponent": "CS",
                "label": "2 to 10 M€",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 38,
                "typeComponent": "CS",
                "label": "10 to 50 M€",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 39,
                "typeComponent": "CS",
                "label": "More than 50 M€",
                "help": ""
            }
        ]
    },
    4: {
        "question": {
            "id": 4,
            "category_id": 13,
            "label": " Do you develop digital services for internal or external use (sales to customers)?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": 5,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": 12,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            }
        ]
    },
    5: {
        "question": {
            "id": 5,
            "category_id": 13,
            "label": " What is the number of users of your digital services?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 46,
                "typeComponent": "Entier",
                "label": "",
                "help": ""
            }
        ]
    },
    6: {
        "question": {
            "id": 6,
            "category_id": 13,
            "label": " Do you apply the rules and best practices for digital accessibility?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    7: {
        "question": {
            "id": 7,
            "category_id": 13,
            "label": " Have you optimized the states and printouts in your application tools (reduced number of pages when printing, ink consumption ...)",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    8: {
        "question": {
            "id": 8,
            "category_id": 13,
            "label": " Do you integrate the principles of the ecodesign of digital services?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    9: {
        "question": {
            "id": 9,
            "category_id": 13,
            "label": " Do you use a modular application architecture?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    10: {
        "question": {
            "id": 10,
            "category_id": 13,
            "label": " Do you do a design review at the end of your application's development?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    11: {
        "question": {
            "id": 11,
            "category_id": 54,
            "label": " What is the overall storage volume of your corporate data (centralized on external hard drives, centralized server, NAS, SAN ...) in Terabytes (TB) useful?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 55,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 56,
                "typeComponent": "CS",
                "label": "I do not want to answer",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 57,
                "typeComponent": "CS_Nombre",
                "label": "x TB ",
                "help": ""
            }
        ]
    },
    12: {
        "question": {
            "id": 12,
            "category_id": 54,
            "label": " Do you have a server or do you only work with one or more workstations?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": 50,
                "id": 60,
                "typeComponent": "CS",
                "label": "We work with workstation (s), without centralized physical server",
                "help": ""
            },
            {
                "idNextQuestion": 13,
                "id": 61,
                "typeComponent": "CS",
                "label": "We have (at least) a centralized physical server",
                "help": ""
            }
        ]
    },
    13: {
        "question": {
            "id": 13,
            "category_id": 54,
            "label": " Do you have a dedicated room, simple room or cupboard with bay dedicated to your IT infrastructure?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": 31,
                "id": 64,
                "typeComponent": "CS",
                "label": "A closet or a room without any specific system",
                "help": ""
            },
            {
                "idNextQuestion": 14,
                "id": 65,
                "typeComponent": "CS",
                "label": "A dedicated room",
                "help": ""
            }
        ]
    },
    14: {
        "question": {
            "id": 14,
            "category_id": 54,
            "label": " Is your computer room in house or at a host?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 68,
                "typeComponent": "CS",
                "label": "Internal",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 69,
                "typeComponent": "CS",
                "label": "Host Member of the European Code of Conduct for Datacenters",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 70,
                "typeComponent": "CS",
                "label": "Non-adhering Host of the European Code of Conduct for Data Centers",
                "help": ""
            }
        ]
    },
    15: {
        "question": {
            "id": 15,
            "category_id": 54,
            "label": " What is the total area of your computer rooms (excluding technical infrastructure *)? (in m2)",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 73,
                "typeComponent": "CS_Nombre",
                "label": "X m2 ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 74,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 75,
                "typeComponent": "CS",
                "label": "I do not want to answer",
                "help": ""
            }
        ]
    },
    16: {
        "question": {
            "id": 16,
            "category_id": 54,
            "label": " Do you know the PUE * of your Data Center?",
            "help": "*PUE : Power Usage Effectiveness"
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 79,
                "typeComponent": "CS",
                "label": "Less than 1,6 ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 80,
                "typeComponent": "CS",
                "label": "Between 1,6 and 2,1 ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 81,
                "typeComponent": "CS",
                "label": "More than 2,1 ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 82,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    17: {
        "question": {
            "id": 17,
            "category_id": 54,
            "label": " What is the rate of charge or energy use of your computer rooms?",
            "help": "Rate = Electrical power absorbed by your IT equipment, divided by room capacity in kW, then multiplied by 100 (used energy / available energy)"
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 86,
                "typeComponent": "CS",
                "label": "100% - 90% ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 87,
                "typeComponent": "CS",
                "label": "90% - 60% ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 88,
                "typeComponent": "CS",
                "label": "Less than 60% ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 89,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    18: {
        "question": {
            "id": 18,
            "category_id": 54,
            "label": " Have you led or are you planning actions to optimize your infrastructure? Especially :",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    19: {
        "question": {
            "id": 19,
            "category_id": 54,
            "label": " The purchase of non-IT equipment from IT rooms (air conditioning, air treatment, inverters, etc.) according to energy efficiency criteria",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    20: {
        "question": {
            "id": 20,
            "category_id": 54,
            "label": " Implementing the good practices of the European Code of Conduct for DataCenter?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    21: {
        "question": {
            "id": 21,
            "category_id": 54,
            "label": " Data center PUE tracking",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    22: {
        "question": {
            "id": 22,
            "category_id": 54,
            "label": " Regular monitoring of environmental indicators of computer rooms",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    23: {
        "question": {
            "id": 23,
            "category_id": 54,
            "label": " Environmental impact analysis of the datacenter in life cycle approach",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    24: {
        "question": {
            "id": 24,
            "category_id": 54,
            "label": " Optimizing the architecture and layout of rooms",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    25: {
        "question": {
            "id": 25,
            "category_id": 54,
            "label": " The urbanization of halls in hot / cold aisles",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    26: {
        "question": {
            "id": 26,
            "category_id": 54,
            "label": " Containment of air flows (corridors)",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    27: {
        "question": {
            "id": 27,
            "category_id": 54,
            "label": " The use of natural cooling sources (freecooling)",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    28: {
        "question": {
            "id": 28,
            "category_id": 54,
            "label": " Implementation of a heat recovery system for computer rooms (heating)",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    29: {
        "question": {
            "id": 29,
            "category_id": 54,
            "label": " The set temperature in the cold corridor remains higher than 24 °",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    30: {
        "question": {
            "id": 30,
            "category_id": 54,
            "label": " The choice of a modular datacenter architecture",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    31: {
        "question": {
            "id": 31,
            "category_id": 54,
            "label": " Have you led or are you planning actions to optimize your infrastructure? Especially :",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    32: {
        "question": {
            "id": 32,
            "category_id": 54,
            "label": " Suspending network equipment",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    33: {
        "question": {
            "id": 33,
            "category_id": 54,
            "label": " Pooling physical equipment",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    34: {
        "question": {
            "id": 34,
            "category_id": 54,
            "label": " Uninstalling unnecessary infrastructure",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    35: {
        "question": {
            "id": 35,
            "category_id": 54,
            "label": " Traceability of material elements (CMDB)",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    36: {
        "question": {
            "id": 36,
            "category_id": 54,
            "label": " The correct sizing of the servers in relation to their use",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    37: {
        "question": {
            "id": 37,
            "category_id": 54,
            "label": " Give priority to ASHRAE 2 compatible equipment",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    38: {
        "question": {
            "id": 38,
            "category_id": 54,
            "label": " A procedure for provisioning and de-provisioning data-processing equipment in datacenters",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    39: {
        "question": {
            "id": 39,
            "category_id": 54,
            "label": " Do you know the number of physical servers and virtual servers in your company?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": 42,
                "id": 115,
                "typeComponent": "CS",
                "label": "No ",
                "help": ""
            },
            {
                "idNextQuestion": 42,
                "id": 116,
                "typeComponent": "CS",
                "label": "I do not want to answer",
                "help": ""
            },
            {
                "idNextQuestion": 40,
                "id": 117,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            }
        ]
    },
    40: {
        "question": {
            "id": 40,
            "category_id": 54,
            "label": " How many physical servers do you have?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 120,
                "typeComponent": "Entier",
                "label": "",
                "help": ""
            }
        ]
    },
    41: {
        "question": {
            "id": 41,
            "category_id": 54,
            "label": " How many virtual servers do you have?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 123,
                "typeComponent": "Entier",
                "label": "",
                "help": ""
            }
        ]
    },
    42: {
        "question": {
            "id": 42,
            "category_id": 54,
            "label": " What will be the evolution of your number of physical servers for 2019? (in% or quantity)",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 126,
                "typeComponent": "CS_Pourcent",
                "label": "In % ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 127,
                "typeComponent": "CS_Entier",
                "label": "In quantity ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 128,
                "typeComponent": "CS",
                "label": "I do not want to answer",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 129,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    43: {
        "question": {
            "id": 43,
            "category_id": 54,
            "label": " What will be the evolution of your number of virtual servers for 2019? (in% or quantity)",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 132,
                "typeComponent": "CS_Pourcent",
                "label": "In % ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 133,
                "typeComponent": "CS_Entier",
                "label": "In quantity ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 134,
                "typeComponent": "CS",
                "label": "I do not want to answer",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 135,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    44: {
        "question": {
            "id": 44,
            "category_id": 137,
            "label": " Has your company appointed a Green IT Manager / Digital Manager?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    45: {
        "question": {
            "id": 45,
            "category_id": 137,
            "label": " Do you have a responsible digital strategy broken down into an action plan?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    46: {
        "question": {
            "id": 46,
            "category_id": 137,
            "label": " Is Green IT a topic integrated into your CSR strategy?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    47: {
        "question": {
            "id": 47,
            "category_id": 137,
            "label": " Do you regularly evaluate the environmental impacts of your information system?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 153,
                "typeComponent": "CS",
                "label": "Yes partially, including only equipment present in the company",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 154,
                "typeComponent": "CS",
                "label": "Yes totally, including our internal equipment and services hosted by third parties",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 155,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 156,
                "typeComponent": "CS",
                "label": "I do not know",
                "help": ""
            }
        ]
    },
    48: {
        "question": {
            "id": 48,
            "category_id": 137,
            "label": " Do you have a team of competent referees on the topics of Green IT?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    49: {
        "question": {
            "id": 49,
            "category_id": 137,
            "label": " Have you integrated Green IT into your business strategy",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    50: {
        "question": {
            "id": 50,
            "category_id": 168,
            "label": " Do you have those equipments in your compagny:",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 169,
                "typeComponent": "CM",
                "label": "Used",
                "help": "equipment used in the business activity"
            },
            {
                "idNextQuestion": null,
                "id": 170,
                "typeComponent": "CM",
                "label": "Not used functional",
                "help": "equipment in working order but no more used by the company (stored)"
            },
            {
                "idNextQuestion": null,
                "id": 171,
                "typeComponent": "CM",
                "label": "Neither used nor functional",
                "help": "out of service equipment (HS) waiting for end of life treatment"
            }
        ]
    },
    51: {
        "question": {
            "id": 51,
            "category_id": 168,
            "label": " Fixed stations, workstations",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 169,
                "typeComponent": "CM",
                "label": "Used",
                "help": "equipment used in the business activity"
            },
            {
                "idNextQuestion": null,
                "id": 170,
                "typeComponent": "CM",
                "label": "Not used functional",
                "help": "equipment in working order but no more used by the company (stored)"
            },
            {
                "idNextQuestion": null,
                "id": 171,
                "typeComponent": "CM",
                "label": "Neither used nor functional",
                "help": "out of service equipment (HS) waiting for end of life treatment"
            }
        ]
    },
    52: {
        "question": {
            "id": 52,
            "category_id": 168,
            "label": " Laptops, digital tablets",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 169,
                "typeComponent": "CM",
                "label": "Used",
                "help": "equipment used in the business activity"
            },
            {
                "idNextQuestion": null,
                "id": 170,
                "typeComponent": "CM",
                "label": "Not used functional",
                "help": "equipment in working order but no more used by the company (stored)"
            },
            {
                "idNextQuestion": null,
                "id": 171,
                "typeComponent": "CM",
                "label": "Neither used nor functional",
                "help": "out of service equipment (HS) waiting for end of life treatment"
            }
        ]
    },
    53: {
        "question": {
            "id": 53,
            "category_id": 168,
            "label": " Small printers (<15kg, potentially used by a household)",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 169,
                "typeComponent": "CM",
                "label": "Used",
                "help": "equipment used in the business activity"
            },
            {
                "idNextQuestion": null,
                "id": 170,
                "typeComponent": "CM",
                "label": "Not used functional",
                "help": "equipment in working order but no more used by the company (stored)"
            },
            {
                "idNextQuestion": null,
                "id": 171,
                "typeComponent": "CM",
                "label": "Neither used nor functional",
                "help": "out of service equipment (HS) waiting for end of life treatment"
            }
        ]
    },
    54: {
        "question": {
            "id": 54,
            "category_id": 168,
            "label": " Flat screen monitors",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 169,
                "typeComponent": "CM",
                "label": "Used",
                "help": "equipment used in the business activity"
            },
            {
                "idNextQuestion": null,
                "id": 170,
                "typeComponent": "CM",
                "label": "Not used functional",
                "help": "equipment in working order but no more used by the company (stored)"
            },
            {
                "idNextQuestion": null,
                "id": 171,
                "typeComponent": "CM",
                "label": "Neither used nor functional",
                "help": "out of service equipment (HS) waiting for end of life treatment"
            }
        ]
    },
    55: {
        "question": {
            "id": 55,
            "category_id": 168,
            "label": " Other flat screens (TV, projection screen, digital board ...)",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 169,
                "typeComponent": "CM",
                "label": "Used",
                "help": "equipment used in the business activity"
            },
            {
                "idNextQuestion": null,
                "id": 170,
                "typeComponent": "CM",
                "label": "Not used functional",
                "help": "equipment in working order but no more used by the company (stored)"
            },
            {
                "idNextQuestion": null,
                "id": 171,
                "typeComponent": "CM",
                "label": "Neither used nor functional",
                "help": "out of service equipment (HS) waiting for end of life treatment"
            }
        ]
    },
    56: {
        "question": {
            "id": 56,
            "category_id": 168,
            "label": " CRT monitors (monitors or other)",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 169,
                "typeComponent": "CM",
                "label": "Used",
                "help": "equipment used in the business activity"
            },
            {
                "idNextQuestion": null,
                "id": 170,
                "typeComponent": "CM",
                "label": "Not used functional",
                "help": "equipment in working order but no more used by the company (stored)"
            },
            {
                "idNextQuestion": null,
                "id": 171,
                "typeComponent": "CM",
                "label": "Neither used nor functional",
                "help": "out of service equipment (HS) waiting for end of life treatment"
            }
        ]
    },
    57: {
        "question": {
            "id": 57,
            "category_id": 168,
            "label": " Video projectors",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 169,
                "typeComponent": "CM",
                "label": "Used",
                "help": "equipment used in the business activity"
            },
            {
                "idNextQuestion": null,
                "id": 170,
                "typeComponent": "CM",
                "label": "Not used functional",
                "help": "equipment in working order but no more used by the company (stored)"
            },
            {
                "idNextQuestion": null,
                "id": 171,
                "typeComponent": "CM",
                "label": "Neither used nor functional",
                "help": "out of service equipment (HS) waiting for end of life treatment"
            }
        ]
    },
    58: {
        "question": {
            "id": 58,
            "category_id": 168,
            "label": " Mobile phones",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 169,
                "typeComponent": "CM",
                "label": "Used",
                "help": "equipment used in the business activity"
            },
            {
                "idNextQuestion": null,
                "id": 170,
                "typeComponent": "CM",
                "label": "Not used functional",
                "help": "equipment in working order but no more used by the company (stored)"
            },
            {
                "idNextQuestion": null,
                "id": 171,
                "typeComponent": "CM",
                "label": "Neither used nor functional",
                "help": "out of service equipment (HS) waiting for end of life treatment"
            }
        ]
    },
    59: {
        "question": {
            "id": 59,
            "category_id": 168,
            "label": " Fixed telephones (standalone not connected to such a standard)",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 169,
                "typeComponent": "CM",
                "label": "Used",
                "help": "equipment used in the business activity"
            },
            {
                "idNextQuestion": null,
                "id": 170,
                "typeComponent": "CM",
                "label": "Not used functional",
                "help": "equipment in working order but no more used by the company (stored)"
            },
            {
                "idNextQuestion": null,
                "id": 171,
                "typeComponent": "CM",
                "label": "Neither used nor functional",
                "help": "out of service equipment (HS) waiting for end of life treatment"
            }
        ]
    },
    60: {
        "question": {
            "id": 60,
            "category_id": 168,
            "label": " Digital cameras",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 169,
                "typeComponent": "CM",
                "label": "Used",
                "help": "equipment used in the business activity"
            },
            {
                "idNextQuestion": null,
                "id": 170,
                "typeComponent": "CM",
                "label": "Not used functional",
                "help": "equipment in working order but no more used by the company (stored)"
            },
            {
                "idNextQuestion": null,
                "id": 171,
                "typeComponent": "CM",
                "label": "Neither used nor functional",
                "help": "out of service equipment (HS) waiting for end of life treatment"
            }
        ]
    },
    61: {
        "question": {
            "id": 61,
            "category_id": 168,
            "label": " Hard Disk Devices, Storage, Backup",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 169,
                "typeComponent": "CM",
                "label": "Used",
                "help": "equipment used in the business activity"
            },
            {
                "idNextQuestion": null,
                "id": 170,
                "typeComponent": "CM",
                "label": "Not used functional",
                "help": "equipment in working order but no more used by the company (stored)"
            },
            {
                "idNextQuestion": null,
                "id": 171,
                "typeComponent": "CM",
                "label": "Neither used nor functional",
                "help": "out of service equipment (HS) waiting for end of life treatment"
            }
        ]
    },
    62: {
        "question": {
            "id": 62,
            "category_id": 168,
            "label": " Do you have other devices in your company?",
            "help": "Keyboards, mouse, graphic tablets, scanners, microphones, speakers, office equipment ..."
        },
        "responses": [
            {
                "idNextQuestion": 63,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": 70,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            }
        ]
    },
    63: {
        "question": {
            "id": 63,
            "category_id": 168,
            "label": " Regarding other devices, do you have in your company:",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 169,
                "typeComponent": "CM",
                "label": "Used",
                "help": "equipment used in the business activity"
            },
            {
                "idNextQuestion": null,
                "id": 170,
                "typeComponent": "CM",
                "label": "Not used functional",
                "help": "equipment in working order but no more used by the company (stored)"
            },
            {
                "idNextQuestion": null,
                "id": 171,
                "typeComponent": "CM",
                "label": "Neither used nor functional",
                "help": "out of service equipment (HS) waiting for end of life treatment"
            }
        ]
    },
    64: {
        "question": {
            "id": 64,
            "category_id": 168,
            "label": " Keyboards",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 169,
                "typeComponent": "CM",
                "label": "Used",
                "help": "equipment used in the business activity"
            },
            {
                "idNextQuestion": null,
                "id": 170,
                "typeComponent": "CM",
                "label": "Not used functional",
                "help": "equipment in working order but no more used by the company (stored)"
            },
            {
                "idNextQuestion": null,
                "id": 171,
                "typeComponent": "CM",
                "label": "Neither used nor functional",
                "help": "out of service equipment (HS) waiting for end of life treatment"
            }
        ]
    },
    65: {
        "question": {
            "id": 65,
            "category_id": 168,
            "label": " Mouse",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 169,
                "typeComponent": "CM",
                "label": "Used",
                "help": "equipment used in the business activity"
            },
            {
                "idNextQuestion": null,
                "id": 170,
                "typeComponent": "CM",
                "label": "Not used functional",
                "help": "equipment in working order but no more used by the company (stored)"
            },
            {
                "idNextQuestion": null,
                "id": 171,
                "typeComponent": "CM",
                "label": "Neither used nor functional",
                "help": "out of service equipment (HS) waiting for end of life treatment"
            }
        ]
    },
    66: {
        "question": {
            "id": 66,
            "category_id": 168,
            "label": " Graphic tablets",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 169,
                "typeComponent": "CM",
                "label": "Used",
                "help": "equipment used in the business activity"
            },
            {
                "idNextQuestion": null,
                "id": 170,
                "typeComponent": "CM",
                "label": "Not used functional",
                "help": "equipment in working order but no more used by the company (stored)"
            },
            {
                "idNextQuestion": null,
                "id": 171,
                "typeComponent": "CM",
                "label": "Neither used nor functional",
                "help": "out of service equipment (HS) waiting for end of life treatment"
            }
        ]
    },
    67: {
        "question": {
            "id": 67,
            "category_id": 168,
            "label": " Scanners",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 169,
                "typeComponent": "CM",
                "label": "Used",
                "help": "equipment used in the business activity"
            },
            {
                "idNextQuestion": null,
                "id": 170,
                "typeComponent": "CM",
                "label": "Not used functional",
                "help": "equipment in working order but no more used by the company (stored)"
            },
            {
                "idNextQuestion": null,
                "id": 171,
                "typeComponent": "CM",
                "label": "Neither used nor functional",
                "help": "out of service equipment (HS) waiting for end of life treatment"
            }
        ]
    },
    68: {
        "question": {
            "id": 68,
            "category_id": 168,
            "label": " Speakers",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 169,
                "typeComponent": "CM",
                "label": "Used",
                "help": "equipment used in the business activity"
            },
            {
                "idNextQuestion": null,
                "id": 170,
                "typeComponent": "CM",
                "label": "Not used functional",
                "help": "equipment in working order but no more used by the company (stored)"
            },
            {
                "idNextQuestion": null,
                "id": 171,
                "typeComponent": "CM",
                "label": "Neither used nor functional",
                "help": "out of service equipment (HS) waiting for end of life treatment"
            }
        ]
    },
    69: {
        "question": {
            "id": 69,
            "category_id": 168,
            "label": " Office automation",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 169,
                "typeComponent": "CM",
                "label": "Used",
                "help": "equipment used in the business activity"
            },
            {
                "idNextQuestion": null,
                "id": 170,
                "typeComponent": "CM",
                "label": "Not used functional",
                "help": "equipment in working order but no more used by the company (stored)"
            },
            {
                "idNextQuestion": null,
                "id": 171,
                "typeComponent": "CM",
                "label": "Neither used nor functional",
                "help": "out of service equipment (HS) waiting for end of life treatment"
            }
        ]
    },
    70: {
        "question": {
            "id": 70,
            "category_id": 200,
            "label": " Do you know the consumption of your workstation in kWh per year?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 201,
                "typeComponent": "CS_Nombre",
                "label": "Yes (please specify how much in kWh / year) ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 202,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            }
        ]
    },
    71: {
        "question": {
            "id": 71,
            "category_id": 200,
            "label": " Do you track the energy consumption of your compagny activities?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    72: {
        "question": {
            "id": 72,
            "category_id": 200,
            "label": " Do you know the share of IT and IT equipment in your company's total energy consumption?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 210,
                "typeComponent": "CS_Pourcent",
                "label": "Yes (please specify how much in %) ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 211,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            }
        ]
    },
    73: {
        "question": {
            "id": 73,
            "category_id": 200,
            "label": " Have you set up a power management system? (automatic shutdown / shutdown of workstations)",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    74: {
        "question": {
            "id": 74,
            "category_id": 218,
            "label": " Do you use copiers from a repackaging industry (second-hand / second-hand)?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    75: {
        "question": {
            "id": 75,
            "category_id": 218,
            "label": " Do you consolidate individual printers to shared printers?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    76: {
        "question": {
            "id": 76,
            "category_id": 218,
            "label": " Have you set up an identification system on printers (to trigger printing)?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    77: {
        "question": {
            "id": 77,
            "category_id": 218,
            "label": " What is the average life of your professional copier / MFP *?",
            "help": "* MFP Multi Fonction Printer"
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 235,
                "typeComponent": "CS",
                "label": "I do not know",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 236,
                "typeComponent": "CS",
                "label": "Do not want to answer",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 237,
                "typeComponent": "CS_Text",
                "label": "x years (please specify) ",
                "help": ""
            }
        ]
    },
    78: {
        "question": {
            "id": 78,
            "category_id": 218,
            "label": " Are your printers set by default in eco mode? Especially :",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    79: {
        "question": {
            "id": 79,
            "category_id": 218,
            "label": " Energy saving (Automatic standby)",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    80: {
        "question": {
            "id": 80,
            "category_id": 218,
            "label": " Black and white by default",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    81: {
        "question": {
            "id": 81,
            "category_id": 218,
            "label": " Default duplex",
            "help": "* MFP Multi Fonction Printer"
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    82: {
        "question": {
            "id": 82,
            "category_id": 218,
            "label": " Default draft mode",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    83: {
        "question": {
            "id": 83,
            "category_id": 218,
            "label": " What is the number of pages printed / day / employee? (A4 equivalent)",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 246,
                "typeComponent": "CS",
                "label": "Less than 10 ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 247,
                "typeComponent": "CS",
                "label": "From 10 to 20 ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 248,
                "typeComponent": "CS",
                "label": "From 20 to 30 ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 249,
                "typeComponent": "CS",
                "label": "More than 30 ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 250,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    },
    84: {
        "question": {
            "id": 84,
            "category_id": 218,
            "label": " Can you specify the number of cartridges / toners:",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 253,
                "typeComponent": "Entier",
                "label": "Cartridges used a year ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 254,
                "typeComponent": "Entier",
                "label": "Cartridges stored in the average business ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 255,
                "typeComponent": "Entier",
                "label": "Toners used a year ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 256,
                "typeComponent": "Entier",
                "label": "Toners stored in the average business ",
                "help": ""
            }
        ]
    },
    85: {
        "question": {
            "id": 85,
            "category_id": 218,
            "label": " Do you organize the separate collection of waste cartridges / toners?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 259,
                "typeComponent": "CS",
                "label": "Yes, to a repackaging industry",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 260,
                "typeComponent": "CS",
                "label": "Yes, towards a recycling channel (destruction)",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 261,
                "typeComponent": "CS",
                "label": "No no separate collection device is planned",
                "help": ""
            }
        ]
    },
    86: {
        "question": {
            "id": 86,
            "category_id": 218,
            "label": " Do you prefer the use of recycled paper?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 264,
                "typeComponent": "CM",
                "label": "Yes, our paper is made from virgin paste",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 265,
                "typeComponent": "CM",
                "label": "Yes, mixed paper",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 266,
                "typeComponent": "CM",
                "label": "Yes, 100% recycled",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 267,
                "typeComponent": "CM",
                "label": "I'm not paying attention",
                "help": ""
            }
        ]
    },
    87: {
        "question": {
            "id": 87,
            "category_id": 218,
            "label": " Do you choose certified paper?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 270,
                "typeComponent": "CM",
                "label": "Yes, FSC",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 271,
                "typeComponent": "CM",
                "label": "Yes, PEFC",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 272,
                "typeComponent": "CM",
                "label": "Yes, Blue Angel",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 273,
                "typeComponent": "CM",
                "label": "Yes, European Label",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 274,
                "typeComponent": "CM",
                "label": "Yes, other ",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 275,
                "typeComponent": "CM",
                "label": "I'm not paying attention",
                "help": ""
            }
        ]
    },
    88: {
        "question": {
            "id": 88,
            "category_id": 218,
            "label": " Do you organize the separate collection of waste paper for recycling?",
            "help": ""
        },
        "responses": [
            {
                "idNextQuestion": null,
                "id": 1,
                "typeComponent": "CS",
                "label": "Yes",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 2,
                "typeComponent": "CS",
                "label": "No",
                "help": ""
            },
            {
                "idNextQuestion": null,
                "id": 3,
                "typeComponent": "CS",
                "label": "I don't know",
                "help": ""
            }
        ]
    }
}