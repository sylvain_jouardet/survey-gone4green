package fr.pe.greenit.survey.survey.domain;

import fr.pe.greenit.survey.survey.enums.TypeComponent;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Response {

    @Id
    private Long id;

    @Enumerated(EnumType.STRING)
    private TypeComponent typeComponent;
    private String label;
    private String help;

    @OneToMany(
            mappedBy = "response",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<QuestionResponse> questionsResponses = new ArrayList<>();

    public Response() {
    }

    public Response(Long id, TypeComponent typeComponent, String label, String help) {
        this.id = id;
        this.typeComponent = typeComponent;
        this.label = label;
        this.help = help;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeComponent getTypeComponent() {
        return typeComponent;
    }

    public void setTypeComponent(TypeComponent typeComponent) {
        this.typeComponent = typeComponent;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getHelp() {
        return help;
    }

    public void setHelp(String help) {
        this.help = help;
    }

}
