package fr.pe.greenit.survey.survey.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Survey {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private User user;
    @ManyToOne
    private Question question;
    @ManyToOne
    private Response response;

    private String userValue;

    private String idPreviousQuestion;

    public Survey() {
    }

    public Survey(User user, Question question, Response response, String userValue, String idPreviousQuestion) {
        this.user = user;
        this.question = question;
        this.response = response;
        this.userValue = userValue;
        this.idPreviousQuestion = idPreviousQuestion;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public String getUserValue() {
        return userValue;
    }

    public void setUserValue(String userValue) {
        this.userValue = userValue;
    }

    public String getIdPreviousQuestion() {
        return idPreviousQuestion;
    }

    public void setIdPreviousQuestion(String idPreviousQuestion) {
        this.idPreviousQuestion = idPreviousQuestion;
    }

    @Override
    public String toString() {
        return "Survey{" +
                "id=" + id +
                ", user=" + user +
                ", question=" + question +
                ", response=" + response +
                ", userValue='" + userValue + '\'' +
                '}';
    }
}
