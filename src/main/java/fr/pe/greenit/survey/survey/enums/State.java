package fr.pe.greenit.survey.survey.enums;

public enum State {

    IN_PROGRESS("IN_PROGRESS"),
    ACHIEVED("ACHIEVED");

    private String value;

    State(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
