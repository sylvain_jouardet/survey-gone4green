package fr.pe.greenit.survey.survey.repository;

import fr.pe.greenit.survey.survey.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
}
