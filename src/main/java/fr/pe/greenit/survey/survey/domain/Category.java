package fr.pe.greenit.survey.survey.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Category {

    @Id
    private Long id;
    private String title;
    private String logo;
    private String color;

    public Category() {
    }

    public Category(Long id, String title, String logo, String color) {
        this.id = id;
        this.title = title;
        this.logo = logo;
        this.color = color;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return Objects.equals(id, category.id) &&
                Objects.equals(title, category.title) &&
                Objects.equals(logo, category.logo) &&
                Objects.equals(color, category.color);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, title, logo, color);
    }
}
