package fr.pe.greenit.survey.survey.repository;

import fr.pe.greenit.survey.survey.domain.Response;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResponseRepository extends JpaRepository<Response, Long> {
}
