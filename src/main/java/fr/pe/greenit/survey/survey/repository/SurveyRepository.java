package fr.pe.greenit.survey.survey.repository;

import fr.pe.greenit.survey.survey.domain.QuestionResponse;
import fr.pe.greenit.survey.survey.domain.Survey;
import fr.pe.greenit.survey.survey.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SurveyRepository extends JpaRepository<Survey, Long> {

    @Query(value = "SELECT MAX(question_id) FROM survey WHERE survey.user_id = :user_id",
            nativeQuery=true
    )
    public Long findLastByUserId(@Param("user_id") String uuid);

    @Query(value = "SELECT * FROM survey WHERE survey.user_id = :user_id and survey.question_id = :question_id",
            nativeQuery=true
    )
    public Survey findResponseByUserIdQuestionId(@Param("user_id") String uuid, @Param("question_id") Long question_id);

}
