package fr.pe.greenit.survey.survey.domain;

import fr.pe.greenit.survey.survey.enums.State;
import fr.pe.greenit.survey.survey.enums.Typology;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Entity
public class User {

    @Id
    private String id;

    @Enumerated(EnumType.STRING)
    private Typology typology;
    @Enumerated(EnumType.STRING)
    private State state;

    public User() {
    }

    public User(String uuid) {
        id = uuid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Typology getTypology() {
        return typology;
    }

    public void setTypology(Typology typology) {
        this.typology = typology;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
