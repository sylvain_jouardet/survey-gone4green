package fr.pe.greenit.survey.survey.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "QuestionResponse")
@Table(name = "question_response")
public class QuestionResponse {

    @EmbeddedId
    private QuestionResponseId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("id")
    private Question question;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("id")
    private Response response;

    private Long idNextQuestion;

    private QuestionResponse() {
    }

    public QuestionResponse(QuestionResponseId id, Question question, Response response, Long idNextQuestion) {
        this.id = id;
        this.question = question;
        this.response = response;
        this.idNextQuestion = idNextQuestion;
    }

    public QuestionResponseId getId() {
        return id;
    }

    public void setId(QuestionResponseId id) {
        this.id = id;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public Long getIdNextQuestion() {
        return idNextQuestion;
    }

    public void setIdNextQuestion(Long idNextQuestion) {
        this.idNextQuestion = idNextQuestion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuestionResponse that = (QuestionResponse) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(question, that.question) &&
                Objects.equals(response, that.response) &&
                Objects.equals(idNextQuestion, that.idNextQuestion);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, question, response, idNextQuestion);
    }
}
