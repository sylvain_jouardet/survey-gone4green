package fr.pe.greenit.survey.survey.service;

import fr.pe.greenit.survey.survey.domain.*;
import fr.pe.greenit.survey.survey.repository.*;
import fr.pe.greenit.survey.survey.resources.ResponseResource;
import fr.pe.greenit.survey.survey.resources.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TopicService {

    @Autowired
    private QuestionResponseRepository questionResponseRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private ResponseRepository responseRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SurveyRepository surveyRepository;

    public List<Topic> getAllTopic() {

        List<Topic> result = new ArrayList<>();
        List<Question> questions = questionRepository.findAll();

        for (Question question: questions) {
            List<QuestionResponse> qrs = questionResponseRepository.findByQuestionId(question.getId());
            List<ResponseResource> responses = new ArrayList<>();
            for (QuestionResponse  qr : qrs) {
                responses.add(new ResponseResource(qr.getIdNextQuestion(), qr.getResponse()));
            }

            result.add(new Topic(question, responses));
        }

        return result;
    }

    public Question getQuestionById(long id) {
        return questionRepository.findById(id).get();
    }

    public Response getResponseById(long id) {
        return responseRepository.findById(id).get();
    }

    public User getUserById(String id) {
        return userRepository.findById(id).get();
    }

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public Long getLastQuestionByUser(String uuid) {
        return surveyRepository.findLastByUserId(uuid);
    }

    public Survey getSurveyByQuestionByUser(String uuid, Long idQuestion) {
        return surveyRepository.findResponseByUserIdQuestionId(uuid, idQuestion);
    }

    public Long getNextQuestion(Long idQuestion, Long idResponse) {
        Long idNextQuestion = questionResponseRepository.findByQuestionResponseId(idQuestion,idResponse);
        if (idNextQuestion == null) {
            return idQuestion + 1L;
        }
        return idNextQuestion;
    }

    public void saveSurveys(List<Survey> surveys) {
        surveyRepository.saveAll(surveys);
    }


}
