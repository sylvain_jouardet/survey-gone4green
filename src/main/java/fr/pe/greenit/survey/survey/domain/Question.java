package fr.pe.greenit.survey.survey.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Question {

    @Id
    private Long id;
    private String label;
    private String help;

    @ManyToOne
    private Category category;

    @OneToMany(
            mappedBy = "question",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<QuestionResponse> questionsResponses = new ArrayList<>();

    public Question() {
    }

    public Question(Long id, String label, String help) {
        this.label = label;
        this.help = help;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getHelp() {
        return help;
    }

    public void setHelp(String help) {
        this.help = help;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", label='" + label + '\'' +
                ", help='" + help + '\'' +
                '}';
    }
}
