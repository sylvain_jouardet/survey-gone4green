package fr.pe.greenit.survey.survey.repository;

import fr.pe.greenit.survey.survey.domain.QuestionResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionResponseRepository extends JpaRepository<QuestionResponse, Long> {

    @Query(value = "SELECT * FROM question_response qr WHERE qr.question_id = :question_id",
            nativeQuery=true
    )
    public List<QuestionResponse> findByQuestionId(@Param("question_id") Long id);

    @Query(value = "SELECT ID_NEXT_QUESTION FROM question_response qr WHERE qr.question_id = :question_id and qr.response_id = :response_id",
            nativeQuery=true
    )
    public Long findByQuestionResponseId(@Param("question_id") Long questionId, @Param("response_id") Long responseId);
}
