package fr.pe.greenit.survey.survey.web;

import fr.pe.greenit.survey.survey.domain.Question;
import fr.pe.greenit.survey.survey.domain.Response;
import fr.pe.greenit.survey.survey.domain.Survey;
import fr.pe.greenit.survey.survey.domain.User;
import fr.pe.greenit.survey.survey.resources.RequestResource;
import fr.pe.greenit.survey.survey.resources.SurveyResource;
import fr.pe.greenit.survey.survey.resources.UserResource;
import fr.pe.greenit.survey.survey.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.util.StringUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
public class SurveyController {


    public static final String hostname = "http://vps613147.ovh.net:8092/";
//    public static final String hostname = "http://localhost:8092/";
    @Autowired
    private TopicService topicService;


    /**
     * Permet de restituer les dernières valeurs d'un utilisateur après la génération d'un lien "share"
     * @param uuid l'uuid du user présent dans l'url
     * @param redirectAttributes les attributs pour pouvoir charger la page correctement avec les données de l'utilisateur
     * @return la redirection vers la page principale
     */
    @RequestMapping(value = "/url/{uuid}", method = RequestMethod.GET)
    public String redirect(@PathVariable String uuid, RedirectAttributes redirectAttributes) {

        Long lastQuestion = topicService.getLastQuestionByUser(uuid);
        Survey survey = topicService.getSurveyByQuestionByUser(uuid,lastQuestion);
        Long lastResponse = survey.getResponse().getId();
        Long idNextQuestion = topicService.getNextQuestion(lastQuestion, lastResponse);

        RequestResource r = new RequestResource(new UserResource(uuid), null );
        redirectAttributes.addFlashAttribute("nextQuestion", idNextQuestion);
        redirectAttributes.addFlashAttribute("uuid", uuid);

        return "redirect:/";
    }


    /**
     * Permet de soumettre les données d'un formulaire pour les enregistrer en base
     * @param request toutes les questions/réponses
     * @return l'url si besoins pour accéder à nouveau aux données
     */
    @PostMapping(value = "/save", consumes = "application/json")
    public ResponseEntity<String> save(@RequestBody RequestResource request) {
        List<SurveyResource> surveysResource = request.getSurveys();
        UserResource userResource = request.getUser();

        User user;
        if (userResource == null || StringUtils.isEmptyOrWhitespace(userResource.getId())) {
            String uuid = UUID.randomUUID().toString();
            user = new User(uuid);
        } else {
            user = topicService.getUserById(userResource.getId());
        }

        List<Survey> surveys = mapSurveysResourceToSurveys(surveysResource, user);
        topicService.saveUser(user);
        topicService.saveSurveys(surveys);

        return new ResponseEntity<>(hostname + "url/" + user.getId(), HttpStatus.OK);

    }

    private List<Survey> mapSurveysResourceToSurveys(List<SurveyResource> surveysResource, User user) {
        List<Survey> result = new ArrayList<>(surveysResource.size());
        for (SurveyResource resource : surveysResource) {
            Question question = topicService.getQuestionById(resource.getIdQuestion());
            Response response = topicService.getResponseById(resource.getIdResponse());
            Survey survey = new Survey(user, question, response, resource.getUserValue(), resource.getIdPreviousQuestion());
            result.add(survey);
        }

        return result;

    }

}


