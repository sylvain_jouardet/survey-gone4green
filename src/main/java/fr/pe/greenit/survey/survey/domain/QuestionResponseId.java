package fr.pe.greenit.survey.survey.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class QuestionResponseId implements Serializable {

    @Column(name ="question_id")
    private Long questionId;
    @Column(name ="response_id")
    private Long responseId;

    private QuestionResponseId() {
    }

    public QuestionResponseId(Long questionId, Long responseId) {
        this.questionId = questionId;
        this.responseId = responseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuestionResponseId that = (QuestionResponseId) o;
        return Objects.equals(questionId, that.questionId) &&
                Objects.equals(responseId, that.responseId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(questionId, responseId);
    }
}
