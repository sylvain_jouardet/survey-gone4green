package fr.pe.greenit.survey.survey.resources;

import fr.pe.greenit.survey.survey.domain.Question;
import fr.pe.greenit.survey.survey.domain.Response;

import java.util.List;

public class Topic {

    private Question question;
    private List<ResponseResource> responses;

    public Topic(Question question, List<ResponseResource> responses) {
        this.question = question;
        this.responses = responses;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public List<ResponseResource> getResponses() {
        return responses;
    }

    public void setResponses(List<ResponseResource> responses) {
        this.responses = responses;
    }
}
