package fr.pe.greenit.survey.survey.resources;

import java.util.Objects;

public class SurveyResource {

    private Long idQuestion;
    private Long idResponse;
    private String idNextQuestion;
    private String idPreviousQuestion;
    private String userValue;

    public SurveyResource() {
    }

    public SurveyResource(Long idQuestion, Long idResponse, String idNextQuestion, String idPreviousQuestion, String userValue) {
        this.idQuestion = idQuestion;
        this.idResponse = idResponse;
        this.idNextQuestion = idNextQuestion;
        this.idPreviousQuestion = idPreviousQuestion;
        this.userValue = userValue;
    }

    public Long getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(Long idQuestion) {
        this.idQuestion = idQuestion;
    }

    public Long getIdResponse() {
        return idResponse;
    }

    public void setIdResponse(Long idResponse) {
        this.idResponse = idResponse;
    }

    public String getIdNextQuestion() {
        return idNextQuestion;
    }

    public void setIdNextQuestion(String idNextQuestion) {
        this.idNextQuestion = idNextQuestion;
    }

    public String getIdPreviousQuestion() {
        return idPreviousQuestion;
    }

    public void setIdPreviousQuestion(String idPreviousQuestion) {
        this.idPreviousQuestion = idPreviousQuestion;
    }

    public String getUserValue() {
        return userValue;
    }

    public void setUserValue(String userValue) {
        this.userValue = userValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SurveyResource that = (SurveyResource) o;
        return Objects.equals(idQuestion, that.idQuestion) &&
                Objects.equals(idResponse, that.idResponse) &&
                Objects.equals(idNextQuestion, that.idNextQuestion) &&
                Objects.equals(idPreviousQuestion, that.idPreviousQuestion) &&
                Objects.equals(userValue, that.userValue);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idQuestion, idResponse, idNextQuestion, idPreviousQuestion, userValue);
    }
}
