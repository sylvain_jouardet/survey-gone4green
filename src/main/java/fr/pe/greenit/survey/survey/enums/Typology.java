package fr.pe.greenit.survey.survey.enums;

public enum Typology {

    // TODO : identifier les différents type
    TEST("TEST");

    private String value;

    Typology(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
