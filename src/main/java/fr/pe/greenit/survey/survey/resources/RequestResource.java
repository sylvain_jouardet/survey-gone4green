package fr.pe.greenit.survey.survey.resources;

import java.util.List;

public class RequestResource {

    private UserResource user;
    private List<SurveyResource> surveys;

    public RequestResource(UserResource user, List<SurveyResource> surveys) {
        this.surveys = surveys;
        this.user = user;
    }

    public List<SurveyResource> getSurveys() {
        return surveys;
    }

    public void setSurveys(List<SurveyResource> surveys) {
        this.surveys = surveys;
    }

    public UserResource getUser() {
        return user;
    }

    public void setUser(UserResource user) {
        this.user = user;
    }
}
