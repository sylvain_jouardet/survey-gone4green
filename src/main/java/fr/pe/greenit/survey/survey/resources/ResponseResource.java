package fr.pe.greenit.survey.survey.resources;

import fr.pe.greenit.survey.survey.domain.Response;
import fr.pe.greenit.survey.survey.enums.TypeComponent;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

public class ResponseResource {

    private Long idNextQuestion;
    private Long id;
    @Enumerated(EnumType.STRING)
    private TypeComponent typeComponent;
    private String label;
    private String help;

    public ResponseResource(Long idNextQuestion, Response response) {
        this.idNextQuestion = idNextQuestion;
        this.id = response.getId();
        this.help = response.getHelp();
        this.label = response.getLabel();
        this.typeComponent = response.getTypeComponent();
    }

    public Long getIdNextQuestion() {
        return idNextQuestion;
    }

    public void setIdNextQuestion(Long idNextQuestion) {
        this.idNextQuestion = idNextQuestion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeComponent getTypeComponent() {
        return typeComponent;
    }

    public void setTypeComponent(TypeComponent typeComponent) {
        this.typeComponent = typeComponent;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getHelp() {
        return help;
    }

    public void setHelp(String help) {
        this.help = help;
    }
}
